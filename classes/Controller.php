<?php require_once('../lib/Mysql.php'); ?>

<?php

class Controller
{
    /**
     * Регистрация
     *
     * @param $request
     */
    public function register($request)
    {

        $name = trim($request->getParam('name'));
        $login = trim($request->getParam('login'));
        $password = trim($request->getParam('password'));

        if ($name && $login && $password) {
            $Mysql = new Mysql;
            $Mysql->addNewUser($name, $login, $password);
        } else {
            echo "Для регистрации необходимо заполнить поля" . PHP_EOL;
        }
    }

    /**
     * Вход
     *
     * @param $request
     * @return bool
     */
    public function login($request)
    {
        $name = trim($request->getParam('name'));
        $login = trim($request->getParam('login'));
        $password = trim($request->getParam('password'));

        $Mysql = new Mysql();
        $statusRegister = $Mysql->checkUserRegister($name, $login, $password);

        if ($statusRegister) {
            setcookie("name", $name, time() + 30600);
            echo "Пользователь успешно зашел как $name" . PHP_EOL;

            return true;
        } else {
            echo "Пользователь $name не смог залогинется, проверте данные" . PHP_EOL;

            return false;
        }
    }

    /**
     * Выход
     */
    public function logout()
    {
        $name = $_COOKIE['name'];
        setcookie("name", $name, time() - 60 * 60 * 24 * 32);
    }

    /**
     * Добавление
     *
     * @param $request
     * @throws Exception
     */
    public function add($request)
    {
        $title = trim($request->getParam('title'));
        $content = trim($request->getParam('content'));
        $file = $files = $request->getUploadedFiles();

        $name = $_COOKIE['name'];
        $Mysql = new Mysql();
        $userId = $Mysql->getUserId($name);

        if ($userId) {
            $Mysql->addRecipe($title, $content, $file, $userId);
        } else {
            echo "Произошла ошибка" . PHP_EOL;
        }
    }

    /**
     * редактирование
     *
     * @return array
     */
    public function edit()
    {
        $name = $_COOKIE['name'];
        $Mysql = new Mysql();
        $userId = $Mysql->getUserId($name);

        if ($userId) {
            $arrResult = $Mysql->getAllRecipeByUserId($userId);

            return $arrResult;
        } else {
            echo "Поьзователь $name не имеет рецептов " . PHP_EOL;
        }
    }

    /**
     * Удаление
     *
     * @param $request
     */
    public function delete($request)
    {
        $id = trim($request->getParam('id'));
        if ($id) {
            $Mysql = new Mysql();
            $Mysql->deleteBiId($id);
        }
    }

    /**
     * Редактирование
     *
     * @param $request
     * @return array
     */
    public function update($request)
    {
        $id = $title = trim($request->getParam('id'));
        if ($id) {
            $Mysql = new Mysql();
            $arrResult = $Mysql->getCurrentRecipeById($id);

            return $arrResult;
        }
    }

    /**
     * редактирование действие
     *
     * @param $request
     * @throws Exception
     */
    public function updateSubmit($request)
    {

        $id = trim($request->getParam('id'));
        $user_id = trim($request->getParam('user_id'));
        $title = trim($request->getParam('title'));
        $content = trim($request->getParam('content'));
        $file = $request->getUploadedFiles();

        $Mysql = new Mysql();
        $Mysql->updateSubmit($id, $user_id, $title, $content, $file);
    }
}