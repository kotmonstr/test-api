<?php

class Mysql
{
    public $host = 'localhost';
    public $mysql_user = 'root';
    public $password = '1';
    public $datebase = 'test_api';
    public $tableUsers = 'users';
    public $tableRecipe = 'recipe';
    public $link;

    /**
     * Mysql constructor.
     */
    function __construct()
    {
        $this->link = mysqli_connect($this->host, $this->mysql_user, $this->password, $this->datebase);
        if (!$this->link) {
            die('Ошибка соединения: ' . mysqli_error() . PHP_EOL);
        }
        //echo 'Успешно соединились' . PHP_EOL;
    }

    // add new user

    /**
     * @param $name
     * @param $login
     * @param $password
     */
    public function addNewUser($name, $login, $password)
    {
        $sql = "INSERT INTO $this->tableUsers ( id, name, login, password) VALUES (null ,'" . $name . "','" . $login . "','" . $password . "')";
        if (mysqli_query($this->link, $sql)) {
            echo "Новый пользователь успешно создан!" . "<br>";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($this->link) . PHP_EOL;
        }
    }

    // check registring status

    /**
     * @param $name
     * @param $login
     * @param $password
     * @return bool
     */
    public function checkUserRegister($name, $login, $password)
    {
        $sql = "SELECT * FROM $this->tableUsers WHERE `name` = '$name' AND `login` = '$login' AND `password`= '$password'";
        $result = mysqli_query($this->link, $sql);
        $row = mysqli_num_rows($result);
        if ($row >= 1) {
            return true;
        } else {
            return false;
        }
    }

    // get user id

    /**
     * @param $name
     * @return bool
     */
    public function getUserId($name)
    {
        $sql = "SELECT * FROM $this->tableUsers WHERE `name` = '$name'";
        $result = mysqli_query($this->link, $sql);
        $row = mysqli_num_rows($result);

        if ($row >= 1) {
            $result = mysqli_fetch_assoc($result);
            if ($result['id']) {
                return $result['id'];
            } else {
                return false;
            }
        }
    }

    /**
     * @param $title
     * @param $content
     * @param $file
     * @param $userId
     * @throws Exception
     */
    public function addRecipe($title, $content, $file, $userId)
    {

        if (empty($file)) {
            throw new Exception('No file has been send');
        }
        $myFile = $file['file'];
        if ($myFile->getError() === UPLOAD_ERR_OK) {
            $uploadFileName = date("d-m-Y-H-i-s") . '.jpg';
            $myFile->moveTo('../uploads/' . $uploadFileName);
        }

        $sql = "INSERT INTO $this->tableRecipe ( id, title, content, image, user_id) VALUES (null ,'" . $title . "','" . $content . "','" . $uploadFileName . "','" . $userId . "')";
        if (mysqli_query($this->link, $sql)) {
            echo "Новый рецепт успешно добавлен!" . "<br>";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($this->link) . PHP_EOL;
        }
    }

    /**
     * @param $userId
     * @return array
     */
    public function getAllRecipeByUserId($userId)
    {
        $arrResult = [];
        $Iterator = 0;

        $sql = "SELECT * FROM $this->tableRecipe WHERE `user_id` = '$userId'";

        if (mysqli_multi_query($this->link, $sql)) {
            do {
                if ($result = mysqli_use_result($this->link)) {
                    while ($row = mysqli_fetch_row($result)) {
                        $arrResult[$Iterator]['id'] = $row[0];
                        $arrResult[$Iterator]['user_id'] = $row[1];
                        $arrResult[$Iterator]['title'] = $row[2];
                        $arrResult[$Iterator]['content'] = $row[3];
                        $arrResult[$Iterator]['image'] = $row[4];
                        $Iterator++;
                    }
                    mysqli_free_result($result);
                }
            } while (mysqli_more_results($this->link));
        }

        return $arrResult;
    }

    /**
     * @param $Id
     */
    public function deleteBiId($Id)
    {
        $sql = "DELETE FROM $this->tableRecipe WHERE `id` = '$Id'";
        if ($this->link->query($sql) === true) {
            echo "Рецепт удален!" . "<br>";
        } else {
            echo "Error deleting record: " . $this->link->error;
        }
    }

    /**
     * @param $id
     * @return array
     */
    public function getCurrentRecipeById($id)
    {
        $arrResult = [];
        $sql = "SELECT * FROM $this->tableRecipe WHERE `id` = '$id'";
        $result = mysqli_query($this->link, $sql);
        $row = mysqli_num_rows($result);

        if ($row >= 1) {
            $result = mysqli_fetch_assoc($result);
            if ($result) {
                $arrResult['id'] = $result['id'];
                $arrResult['title'] = $result['title'];
                $arrResult['content'] = $result['content'];
                $arrResult['image'] = $result['image'];
                $arrResult['user_id'] = $result['user_id'];
            }
        }

        return $arrResult;
    }

    /**
     * @param $id
     * @param $userId
     * @param $title
     * @param $content
     * @param $file
     * @throws Exception
     */
    public function updateSubmit($id, $userId, $title, $content, $file)
    {

        if (empty($file)) {
            throw new Exception('No file has been send');
        }
        $myFile = $file['file'];
        if ($myFile->getError() === UPLOAD_ERR_OK) {
            $uploadFileName = date("d-m-Y-H-i-s") . '.jpg';
            $myFile->moveTo('../uploads/' . $uploadFileName);
        }

        $sql = "UPDATE $this->tableRecipe SET user_id= '" . $userId . "', title='" . $title . "', content= '" . $content . "', image='" . $uploadFileName . "' WHERE `id`=$id";
        if ($this->link->query($sql) === true) {
            echo "Рецепт успешно отредактирован!" . "<br>";
        } else {
            echo "Error updating record: " . $this->link->error;
        }
    }
}