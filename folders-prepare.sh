#!/usr/bin/env bash

sudo chmod 777 -R logs

set -x
mkdir ./uploads/
chmod -v 0777 ./uploads
