<?php

use Slim\Http\Request;
use Slim\Http\Response;

require_once('../classes/Controller.php');

// Routes

/*
 * Главная страница
 */
$app->get('/', function (Request $request, Response $response, array $args) {
    $this->logger->info("test-api '/' route");

    return $this->renderer->render($response, 'index.phtml', ['title' => "Главная"]);
});

/*
 * Регистрация пользователя
 */
$app->get('/register', function (Request $request, Response $response, array $args) {
    $this->logger->info("test-api '/register' route");
    $user = new Controller;
    $user->register($request);

    return $this->renderer->render($response, 'register.phtml', ['title' => "Регистрация"]);
});

/*
 * Вход пользователя
 */
$app->get('/login', function (Request $request, Response $response, array $args) {
    $this->logger->info("test-api '/login' route");
    $user = new Controller;
    $result = $user->login($request);

    if ($result) {
        return $response->withRedirect('/');
    }

    return $this->renderer->render($response, 'login.phtml', ['title' => "Вход"]);
});

/*
 * Выход пользователя
 */
$app->get('/logout', function (Request $request, Response $response, array $args) {
    $this->logger->info("test-api '/logout' route");

    $user = new Controller;
    $user->logout();

    return $response->withRedirect('/');
});

/**
 * Добавление
 */
$app->get('/add', function (Request $request, Response $response, array $args) {
    $this->logger->info("test-api '/add' route");


    return $this->renderer->render($response, 'add.phtml', ['title' => "Добавить"]);
});

/**
 * Добавление действие
 */
$app->post('/add', function (Request $request, Response $response, array $args) {
    $this->logger->info("test-api '/add' route");

    $user = new Controller;
    $user->add($request);

    return $this->renderer->render($response, 'add.phtml', ['title' => "Добавить"]);
});

/*
 * Редактирование
 */
$app->get('/edit', function (Request $request, Response $response, array $args) {
    $this->logger->info("test-api '/edit' route");

    $user = new Controller;
    $arrRecipes = $user->edit($request);

    return $this->renderer->render($response, 'edit.phtml', ['title' => "Редактировать", 'arrRecipes' => $arrRecipes]);
});

/*
 * Редактирование
 */
$app->get('/update', function (Request $request, Response $response, array $args) {
    $this->logger->info("test-api '/update' route");

    $user = new Controller;
    $arrCurrentRecipe = $user->update($request);

    return $this->renderer->render($response, 'update.phtml', ['title' => "Редактировать", 'arrCurrentRecipe' => $arrCurrentRecipe]);
});
/*
 * Редактирование действие
 */
$app->post('/update', function (Request $request, Response $response, array $args) {
    $this->logger->info("test-api '/update' route");

    $user = new Controller;
    $user->updateSubmit($request);

    return $response->withRedirect('/edit');
});

/*
 * Удаление
 */
$app->get('/delete', function (Request $request, Response $response, array $args) {
    $this->logger->info("test-api '/delete' route");

    $user = new Controller;
    $user->delete($request);

    return $response->withRedirect('/edit');
});

